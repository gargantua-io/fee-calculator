package main

//todo: except our own node & saving data to file

import (
	"encoding/json"
	"fmt"
	"github.com/MinterTeam/minter-go-sdk/api"
	"strconv"
)

func main() {
	client := api.NewApi("https://api.minter.stakeholder.space/")

	obj := height(client, api.LatestBlockHeight)

	e, err := json.Marshal(obj)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(e))

}

func height(client *api.Api, height int) Block {

	validators, _ := client.Validators(height)

	var avg float64 = 0

	var list []Validator
	for i := 0; i < len(validators); i++ {
		//if validators[i].PubKey == ex {
		//	continue
		//}

		v, _ := client.Candidate(validators[i].PubKey, height)

		fee, _ := strconv.Atoi(v.Commission)
		power, _ := strconv.Atoi(validators[i].VotingPower)

		avg += float64(fee) * float64(power) / 100000000

		list = append(list, Validator{
			Key:   validators[i].PubKey,
			Fee:   fee,
			Power: float64(power) / 100000000,
		})

	}

	//fmt.Println(avg)

	return Block{
		Height: height,
		Fee:    avg,
		Data:   list,
	}
}

type Validator struct {
	Key   string  `json:"key"`
	Fee   int     `json:"fee"`
	Power float64 `json:"power"`
}

type Block struct {
	Height int         `json:"height"`
	Fee    float64     `json:"fee"`
	Data   []Validator `json:"data"`
}

// 100 000 000
// 99999983
